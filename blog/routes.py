import os
import secrets

from PIL import Image
from flask import render_template, flash, redirect, url_for, request, abort
from flask_login import login_user, current_user, logout_user, login_required
from werkzeug.datastructures import FileStorage

from blog import app, bcrypt, db
from blog.forms import RegistrationForm, LoginForm, UpdateAccountForm, NewPostForm
from blog.models import User, Post


@app.route('/')
@app.route('/home')
def index():
    posts = Post.query.all()
    return render_template('index.html', posts=posts)


@app.route('/about')
def about():
    return render_template('about.html', title='About Page')


@app.route('/register', methods=['POST', 'GET'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        password_hash = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(first_name=form.first_name.data,
                    last_name=form.last_name.data,
                    username=form.username.data.lower(),
                    email=form.email.data.lower(),
                    password=password_hash)
        db.session.add(user)
        db.session.commit()
        flash(f'Account created for {form.username.data}', category='success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['POST', 'GET'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data.lower()).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            flash(message="You have logged in successfully!", category='success')
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('index'))
        else:
            flash(message="Unsuccessful login!. check email and password", category="danger")
    return render_template('login.html', title='Login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


def __save_photo(form_photo: FileStorage) -> str:
    random_hex = secrets.token_hex(8)
    _, file_ext = os.path.splitext(form_photo.filename)
    file_name = f"{random_hex}{file_ext}"
    file_path = os.path.join(app.root_path, 'static', 'profile_photos', file_name)
    output_size = (125, 125)
    i = Image.open(form_photo)
    i.thumbnail(output_size)
    i.save(file_path)
    assert os.path.isfile(file_path), file_path
    return file_name


def __delete_old_photo(old_photo: str):
    file_path = os.path.join(app.root_path, 'static', 'profile_photos', old_photo)
    assert os.path.isfile(file_path), file_path
    os.remove(file_path)


@app.route('/account', methods=['POST', 'GET'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.profile_photo.data:
            old_photo = current_user.image_file
            __delete_old_photo(old_photo)
            photo_file = __save_photo(form.profile_photo.data)
            current_user.image_file = photo_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.first_name = form.first_name.data
        current_user.last_name = form.last_name.data
        db.session.commit()
        flash(f'Your Account has been updated', category='success')
        return redirect(url_for('account'))
    elif request.method == 'GET':
        __set_default_form(form)
    image_file = url_for('static', filename=f"profile_photos/{current_user.image_file}")
    return render_template('account.html', title='Account', image_file=image_file, form=form)


@app.route('/post/new', methods=['POST', 'GET'])
@login_required
def new_post():
    form = NewPostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash(f'Your post has been created', category='success')
        return redirect(url_for('index'))
    return render_template('process_post.html', title='New Post', form=form, legend='New Post')


@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)


@app.route('/post/<int:post_id>/update', methods=['POST', 'GET'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = NewPostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        return redirect(url_for('index'))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('process_post.html', title='Update Post', form=form, legend="Update Post")


@app.route('/post/<int:post_id>/delete', methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)
    db.session.commit()
    flash(f'Your post has been deleted successfully!', category='success')
    return redirect(url_for('index'))


def __set_default_form(form):
    form.username.data = current_user.username
    form.email.data = current_user.email
    form.first_name.data = current_user.first_name
    form.last_name.data = current_user.last_name
